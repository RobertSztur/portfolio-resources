const myHeading = document.getElementById('myHeading');
const myList = document.getElementsByTagName('li');
const myColor = document.getElementsByClassName('red');
const myParagraph = document.querySelector('p.description');

myHeading.addEventListener('click', ()=> {
    myHeading.style.color = 'red';
});

for(let i=0; i< myColor.length; i+= 1){
    myColor[i].style.color = 'red';
}