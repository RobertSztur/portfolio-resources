var xsize;
            var ysize;
            var pointsCounter;
            var failCounter;
            var array;
            var time;
            var fieldsArray;
            var valuesArray;
            var numberToSelect;
            var gameIsStarted = false;
            var yCounter;
            var xCounter;

            document.getElementById("btnCustomBoard").addEventListener("click", newCustomBoard, false);
            document.getElementById("btnDefaultBoard").addEventListener("click", newDefaultBoard, false);
            document.getElementById("btnWriteln").addEventListener("click", writelnTest, false);
            var btnNewGame = document.getElementById("btnNewGame");
            btnNewGame.addEventListener("click", newGame, false);

            function newCustomBoard() {
                while (true) {
                    xsize = parseInt(window.prompt("Podaj rozmiar x (5-25):"), 10);
                    if (xsize >= 5 && xsize <= 25) {
                        break;
                    }
                }
                while (true) {
                    ysize = parseInt(window.prompt("Podaj rozmiar y (5-25):"), 10);
                    if (ysize >= 5 && ysize <= 25) {
                        break;
                    }
                }
                newBoard();
            }

            function newDefaultBoard() {
                xsize = 8;
                ysize = 8;
                newBoard();
            }

            function newBoard() {
                gameIsStarted = false;

                array = new Array(xsize);
                for (var i = 0; i < 10; i++) {
                    array[i] = new Array(ysize);
                }

                document.getElementById("btnNewGame").style.display = "inline";
                array = fillArrayRandom(xsize, ysize);
                drawArray(array, xsize, ysize);
                generateHandlers();

                document.getElementById("numberBlock").style.display = "none";
            }

            function newGame() {
                drawArray(array, xsize, ysize);
                generateHandlers();
                numberToSelect = Math.floor(Math.random() * 10);
                document.getElementById("numberBlock").innerHTML = "Wylosowana liczba: <b>" + numberToSelect + "</b>";
                document.getElementById("numberBlock").style.display = "inline";
                zeroArray();
                time = (new Date).getTime();

                gameIsStarted = true;
                failCounter = 0;
                pointsCounter = 0;
            }

            function fillArrayRandom(xsize, ysize) {
                var a = new Array(10);
                for (var i = 0; i < 10; i++) {
                    a[i] = new Array(10);
                }
                for (i = 0; i < xsize; i++) {
                    for (var j = 0; j < ysize; j++) {
                        a[i][j] = Math.floor(Math.random() * 10);
                    }
                }
                return a;
            }

            function drawArray(a, xsize, ysize) {
                var blockText = "";

                blockText += "<table>";
                for (i = 0; i < xsize; i++) {
                    blockText += "<tr>";
                    for (var j = 0; j < ysize; j++) {
                        blockText += "<td id=\"link" + (100 * i + j) + "\">" + a[i][j] + "<//td>";
                    }
                    blockText += "<//tr>";
                }
                blockText += "<//table>";

                document.getElementById("tableBlock").innerHTML = blockText;
            }

            function generateHandlers() {

                fieldsArray = new Array(xsize);
                for (var i = 0; i < 10; i++) {
                    fieldsArray[i] = new Array(ysize);
                }

                for (i = 0; i < xsize; i++) {
                    for (var j = 0; j < ysize; j++) {
                        var x = i;
                        var y = j;
                        fieldsArray[i][j] = document.getElementById("link" + (i * 100 + j));
                        //  tym wypadku może też zadziałać: array[i][j].addEventListener("click", handleTDClick.bind(this, i, j) ,false);
                        fieldsArray[i][j].addEventListener("click", (function (i, j) {
                            return function () {
                                handleTDClick(i, j);
                            };
                        })(i, j), false);
                    }
                }
            }

            function handleTDClick(x, y) {
                if (gameIsStarted) {
                    document.getElementById("link" + (100 * x + y)).style.color = "white";
                    if (valuesArray[x][y] == 1) {
                        document.getElementById("link" + (100 * x + y)).style.background = "green";
                        pointsCounter++;
                    }
                    else {
                        document.getElementById("link" + (100 * x + y)).style.background = "red";
                        failCounter++;
                    }

                    valuesArray[x][y] = 0;
                    checkIfDone();
                }
            }

            function zeroArray() {
                valuesArray = new Array(xsize);
                for (var i = 0; i < xsize; i++) {
                    valuesArray[i] = new Array(ysize);
                }
                for (i = 0; i < xsize; i++) {
                    for (var j = 0; j < ysize; j++) {
                        if (array[i][j] == numberToSelect) {
                            valuesArray[i][j] = 1;
                        }
                        else {
                            valuesArray[i][j] = 0;
                        }
                    }
                }
            }

            function checkIfDone() {
                if (arrayIsEmpty(valuesArray)) {
                    finalAnimation();
                    var gameTime = ((new Date).getTime() - time) / 1000;
                    var endText = "Zakończono grę!\n" + "Czas: " + gameTime + "s" + "\nIlość błędów: " + failCounter + "\nIlość punktów: ";
                    endText += pointsCounter + "\nProcent błędów: " + (failCounter / pointsCounter) + "\nŚredni czas: ";
                    endText += (gameTime / pointsCounter).toPrecision(2) + "s";
                    window.alert(endText);
                    endText = "Zakończono grę!<br>" + "Czas: " + gameTime + "s" + "<br>Ilość błędów: " + failCounter + "<br>Ilość punktów: ";
                    endText += pointsCounter + "<br>Procent błędów: " + (failCounter / pointsCounter) + "<br>Średni czas: ";
                    endText += (gameTime / pointsCounter).toPrecision(2) + "s";
                    footerBlock.innerHTML = endText;
                    gameIsStarted = false;
                }
                //document.getElementById("footerBlock").innerHTML = "E of cid";
            }

            function arrayIsEmpty(array) {
                for (var i = 0; i < xsize; i++) {
                    for (var j = 0; j < ysize; j++) {
                        if (array[i][j] == 1) {
                            return false;
                        }
                    }
                }
                return true;
            }

            function finalAnimation() {
                for (xCounter = 0; xCounter < xsize; xCounter++) {
                    for (yCounter = 0; yCounter < ysize; yCounter++) {
                        if (array[xCounter][yCounter] != numberToSelect) {
                            fieldsArray[xCounter][yCounter].style.background = "#808080";
                        }

                    }
                }
            }

            function writelnTest() {
                document.write("Wiek użytkownika: " + parseFloat(window.prompt("Podaj wiek(float): ")));
            }




