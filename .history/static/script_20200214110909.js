var student;
var message = "";

function print(message){
    var outputDiv = document.getElementById('output');
    outputDiv.innerHTML = message;
}

var students = [
{name: 'Robert',
track: 'Java',
skill: 'intermediate'},

{name: 'Wiola',
track: 'JavaScript',
skill: 'beginner'},

{name: 'Jacek',
track: 'Java',
skill: 'advance'},
];

for(var i =0; i < students.length; i++ ){
student = students[i];
message += '<h2>Student: ' + student.name + '</h2>';
}
print(message);

