(function(){
    var btn = document.querySelector("#getNumbers");
    output = document.querySelector("#showNumbers");

    function getRandom(min,max){
        return Math.round(Math.random(max-min)+min);
    }

    function showRandomNumbers(){
        var numbers = [];
        for (let index = 0; index < 6; index++) {
            random = getRandom(1,49);

            while(numbers.indexOf(random) !== -1){
                random = getRandom(1,49);
            }

            numbers.push(random);
        }
         console.log(numbers);
    }

    btn.onclick = showRandomNumbers;
})();